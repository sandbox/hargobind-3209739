<?php

/**
 * @file
 * Search API integration for Custom Search Paths.
 */

include_once 'search_api_custom_paths.entities.inc';

/**
 * Implements hook_menu().
 */
function search_api_custom_paths_menu() {
  $items = array();

  $entity_type = 'custom_search_path';
  $entity_ui_path = 'admin/config/search/search_api/csp/manage/%entity_object';
  $id_count = count(explode('/', $entity_ui_path)) - 1;
  $items[$entity_ui_path . '/view'] = array(
    'title' => 'View',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('custom_search_path_view', $entity_type, $id_count),
    'load arguments' => array($entity_type),
    'access callback' => 'entity_access',
    'access arguments' => array('view', $entity_type, $id_count),
    'file' => 'search_api_custom_paths.entities.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_cron().
 */
function search_api_custom_paths_cron() {
  $entity_type = 'custom_search_path';

  // Find all pages which need refreshing.
  $records = db_query("
    SELECT csp_id
    FROM {search_api_custom_search_paths}
    WHERE (refresh_interval > 0) AND (CURRENT_TIMESTAMP() > last_refresh + (refresh_interval * 60))
  ")->fetchCol(0);

  if (!empty($records)) {
    $entities = entity_load($entity_type, $records);
    foreach ($entities as $entity) {
      // Retrieve the page content.
      $entity->getPageContent(TRUE);
    }
  }
}

/**
 * Implements hook_entity_update().
 *
 * Respond to entity updates to refresh the content in CSP entities.
 */
function search_api_custom_paths_entity_update($entity, $entity_type) {
  global $user;
  $csp_type = 'custom_search_path';

  // Skip CSP entities.
  if ($entity_type == $csp_type) {
    return;
  }

  // Find all pages that match this URI.
  $entity_uri = entity_uri($entity_type, $entity);
  if (empty($entity_uri['path'])) {
    return;
  }
  $records = db_query("
    SELECT csp_id
    FROM {search_api_custom_search_paths}
    WHERE search_path = :uri
  ", array(':uri' => $entity_uri['path'])
  )->fetchCol(0);

  // For all CSP entities with a matching path, retrieve their page content.
  if (!empty($records)) {
    // Clear the entity cache to make sure we get the fresh content.
    $entity_id = current(entity_extract_ids($entity_type, $entity));
    entity_get_controller($entity_type)->resetCache(array($entity_id));

    $entities = entity_load($csp_type, $records);
    foreach ($entities as $entity) {
      $entity->getPageContent(TRUE);
    }
  }
}

/**
 * Implements hook_search_api_index_items_alter().
 *
 * Remove items from the index that have no content.
 *
 * Please be aware that generally preventing the indexing of certain items is
 * deprecated. This is better done with data alterations, which can easily be
 * configured and only added to indexes where this behaviour is wanted.
 * If your module will use this hook to reject certain items from indexing,
 * please document this clearly to avoid confusion.
 *
 * @param array $items
 *   The entities that will be indexed (before calling any data alterations).
 * @param SearchApiIndex $index
 *   The search index on which items will be indexed.
 */
function search_api_custom_paths_search_api_index_items_alter(array &$items, SearchApiIndex $index) {
  $csp_type = 'custom_search_path';
  $multi_types = $index->item_type == 'multiple';
  foreach ($items as $id => $item) {
    $csp = $item;
    if ($multi_types) {
      if ($item->item_type !== $csp_type) {
        continue;
      }
      $csp = $item->{$csp_type};
    }
    else if ($index->item_type !== $csp_type) {
      continue;
    }

    // If there is no content, remove the item from the index.
    if ($csp->page_content === NULL) {
      unset($items[$id]);
    }
  }
}
