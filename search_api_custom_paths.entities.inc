<?php

/**
 * @file
 * Entity code for Custom Search Paths.
 */

/**
 * Class for custom_search_path entity.
 */
class SearchApiCustomSearchPath extends Entity {

  // -----------------------------------------------------------------------
  // Base Class overridden methods

  public function save() {
    if (empty($this->created)) {
      $this->created = REQUEST_TIME;
    }
    $this->changed = REQUEST_TIME;

    $ret = parent::save();
    return $ret;
  }

  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  protected function defaultLabel() {
    return $this->title;
  }

  /**
   * Specify the default URI, which is picked up by uri() by default.
   *
   * @param string $op
   *   The operation to perform (e.g. view, delete, or '' to edit).
   */
  protected function defaultUri($op = 'view') {
    if (!empty($op)) {
      $op = '/' . $op;
    }
    return array(
      'path' => 'admin/config/search/search_api/csp/manage/' . $this->identifier() . $op,
    );
  }

  /**
   * Implements buildContent().
   *
   * Add custom fields to the output.
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $result = array(
      'content' => array(
        '#markup' => $this->page_content,
      ),
    );
    return $result;
  }

  /**
   * Get the page content.
   */
  public function getPageContent($retrieve_content = FALSE) {
    global $user;

    // If it's not saved, retrieve it.
    if ($retrieve_content || !isset($this->page_content)) {
      if (!$this->_retrievePageContent()) {
        return FALSE;
      }
    }

    return $this->page_content;
  }

  /**
   * Retrieve the page content and save the entity.
   *
   * This is an internal function.
   */
  protected function _retrievePageContent() {
    global $user;
    $content = NULL;

    // Retrieve content from an external URL.
    if (strpos($this->search_path, '://') !== FALSE) {
      // Retrieve the page contents.
      $result = drupal_http_request($this->search_path);

      if ($result->code != 200) {
        watchdog('search_api_custom_paths', 'HTTP error occurred while trying to fetch %remote. Error: %error',
          array('%remote' => $this->search_path, '%error' => $result->code . ' ' . $result->error),
          WATCHDOG_NOTICE, l('edit', $this->defaultUri('')['path'])
        );
      }
      else if ($result->data) {
        // Attempt to just get the contents of the <body> tag.
        preg_match("/<body[^>]*>(.*?)<\/body>/is", $result->data, $matches);
        if (!empty($matches[1])) {
          $content = $matches[1];
        }
        else {
          $content = $result->data;
        }
      }
    }
    // Build the page contents using the standard menu router execution. The
    // global user is temporarily switched to Anonymous while retrieving the
    // page content so that it is built without elevated permissions.
    else {
      // Make a copy of the current user.
      $current_user = clone($user);
      if ($current_user->uid > 0) {
        // Make the global user anonymous.
        $user = user_load(0);

        // Clear the menu item cache.
        drupal_static_reset('menu_get_item');
      }

      // Generate the content using the standard Drupal method.
      $search_path = drupal_get_normal_path($this->search_path);
      $result = menu_execute_active_handler($search_path, FALSE);

      // If the current user is not anonymous, restore the user object.
      if ($current_user->uid > 0) {
        $user = $current_user;
      }

      // The results of menu_execute_active_handler() are typically an integer
      // when an error is encountered, e.g. MENU_ACCESS_DENIED.
      if (is_int($result)) {
        watchdog('search_api_custom_paths', 'Unable to retrieve the content for the path: %path. It may be unpublished, or anonymous users may not have access to it.',
          array('%path' => url($this->search_path)), WATCHDOG_NOTICE, l('edit', $this->defaultUri('')['path']));
      }
      else {
        // Some content may be a string, some may be a render array. Convert
        // everything to a render array to make it simpler to process.
        if (!is_array($result)) {
          $result = array(
            'content' => array(
              '#markup' => $result,
            ),
          );
        }

        // Render the results.
        $content = drupal_render($result);
      }
    }

    $this->page_content = $content;
    $this->last_refresh = REQUEST_TIME;

    $this->save();

    return $content != NULL;
  }
};

/**
 * Implements hook_entity_info
 */
function search_api_custom_paths_entity_info() {
  return array(
    'custom_search_path' => array(
      'label' => t('Custom search path'),
      'module' => 'search_api_custom_paths',
      'entity class' => 'SearchApiCustomSearchPath',
      'controller class' => 'EntityAPIController',
      'views controller class' => 'EntityDefaultViewsController',
      'base table' => 'search_api_custom_search_paths',
      'entity keys' => array(
        'id' => 'csp_id',
        'label' => 'title',
      ),
      'access callback' => 'search_api_custom_paths_entity_access',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'admin ui' => array(
        'path' => 'admin/config/search/search_api/csp',
        'controller class' => 'EntityDefaultUIController',
      ),
    ),
  );
}

/**
 * Entity access callback.
 */
function search_api_custom_paths_entity_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer search_api', $account);
}

/**
 * Implements hook_entity_property_info().
 *
 * @see entity_entity_property_info()
 */
function search_api_custom_paths_entity_property_info() {
  $info = array();

  // Add meta-data about the basic entity properties.
  $properties = &$info['custom_search_path']['properties'];

  $properties['csp_id'] = array(
    'label' => t('Search path ID'),
    'description' => t('The unique ID of the custom search path.'),
    'type' => 'integer',
    'schema field' => 'csp_id',
  );
  $properties['title'] = array(
    'label' => t('Title'),
    'description' => t('The title of the page. This will be shown in search results.'),
    'type' => 'text',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'title',
    'required' => TRUE,
  );
  $properties['search_path'] = array(
    'label' => t('Search path'),
    'description' => t('The URL of the path to index.'),
    'type' => 'uri',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'search_path',
    'required' => TRUE,
  );
  $properties['page_content'] = array(
    'label' => t('Page content'),
    'description' => t('The contents of the page at the custom search path.'),
    'type' => 'text',
    'sanitized' => TRUE,
    'raw getter callback' => 'entity_property_verbatim_get',
    'getter callback' => 'search_api_custom_paths_get_entity_properties',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'page_content',
  );
  $properties['refresh_interval'] = array(
    'label' => t('Refresh interval'),
    'description' => t('How often to refresh the contents of the page in the search index.'),
    'type' => 'integer',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'refresh_interval',
  );
  $properties['last_refresh'] = array(
    'label' => t('Date last refreshed'),
    'description' => t('The date when the custom search path was last refreshed.'),
    'type' => 'date',
    'schema field' => 'last_refresh',
  );
  $properties['created'] = array(
    'label' => t('Date created'),
    'description' => t('The date when the custom search path was created.'),
    'type' => 'date',
    'setter callback' => 'entity_property_verbatim_set',
    'schema field' => 'created',
  );
  $properties['changed'] = array(
    'label' => t('Date changed'),
    'description' => t('The date when the custom search path was most recently updated.'),
    'type' => 'date',
    'schema field' => 'changed',
  );

  return $info;
}

/**
 * Callback for getting custom properties.
 *
 * @see search_api_custom_paths_entity_property_info()
 */
function search_api_custom_paths_get_entity_properties($entity, array $options, $name) {
  switch ($name) {
    case 'page_content':
      // Add a space after closing HTML tags to prevent lumping together of
      // text in consecutive HTML tags.
      $page_content = $entity->getPageContent();
      $page_content = preg_replace('#(</[a-zA-Z0-9_]+>)#', '$1 ', $page_content);
      return filter_xss($page_content);
  }
}

/**
 * Entity edit form to create/edit a custom_search_path.
 */
function custom_search_path_form($form, &$form_state, $entity, $op = 'edit') {
  $entity_type = entity_get_type($entity);
  $entity_id = current(entity_extract_ids($entity_type, $entity));

  $form['entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#description' => t('The title of the page. This will be shown in search results.'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->title) ? $entity->title : NULL,
    '#required' => TRUE,
  );
  $form['search_path'] = array(
    '#title' => t('Search path'),
    '#description' => t('The URL of the path to index.'),
    '#type' => 'textfield',
    '#default_value' => isset($entity->search_path) ? drupal_get_path_alias($entity->search_path) : NULL,
    '#required' => TRUE,
  );
  $interval_minutes = array(15, 30, 45, 60, 360, 1440, 10080, 43200);
  $interval_options = array(0 => t('On entity update'));
  foreach ($interval_minutes as $interval) {
    $minutes = $interval * 60;
    $interval_options[$minutes] = format_interval($minutes);
  }
  $form['refresh_interval'] = array(
    '#title' => t('Refresh interval'),
    '#description' => t('How often to refresh the content. Choosing "On entity update" means the cached content will only update when the page (node, taxonomy term, etc.) is updated.'),
    '#type' => 'select',
    '#options' => $interval_options,
    '#default_value' => isset($entity->refresh_interval) ? $entity->refresh_interval : NULL,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  if ($entity_id) {
    $form['entity_id'] = array(
      '#type' => 'hidden',
      '#value' => $entity_id,
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('custom_search_path_form_delete_submit'),
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'custom_search_path_form_validate';

  return $form;
}

/**
 * Form submission handler for the 'Delete' button on path_admin_form().
 *
 * @see path_admin_form_validate()
 * @see path_admin_form_submit()
 */
function custom_search_path_form_delete_submit($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array($entity->defaultUri('delete')['path'], array('query' => $destination));
}

/**
 * Validation callback for custom_search_path_form().
 */
function custom_search_path_form_validate($form, &$form_state) {
  // Help the user by removing the base URL to make the URL internal.
  $search_path = &$form_state['values']['search_path'];
  if (strpos($search_path, '://') !== FALSE) {
    $site_url = url('', array('absolute' => TRUE));
    $search_path = str_replace($site_url, '', $search_path);
  }
  // Normalize the path.
  $search_path = drupal_get_normal_path($search_path);

  if (!drupal_valid_path($search_path)) {
    form_set_error('search_path', t("The path '@link_path' is either invalid or you do not have access to it.", array('@link_path' => $search_path)));
  }
}

/**
 * Submit callback for custom_search_path_form().
 */
function custom_search_path_form_submit($form, &$form_state) {
  $entity = entity_ui_form_submit_build_entity($form, $form_state);

  // Retrieve the page content, which performs a save.
  $entity->getPageContent(TRUE);

  drupal_set_message(t('Custom search path saved.'));
  $form_state['redirect'] = 'admin/config/search/search_api/csp';
}

/**
 * Callback function for viewing an individual entity.
 */
function custom_search_path_view($form, &$form_state, $entity_type, $entity) {
  drupal_set_title($entity->label());

  $search_path = $entity->search_path;
  if (strpos($search_path, '://') === FALSE) {
    $search_path = url(drupal_get_path_alias($search_path));
  }
  $form['search_path'] = array(
    '#title' => t('Search path URL'),
    '#type' => 'item',
    '#markup' => l($search_path, $search_path),
  );

  $form['refresh_interval'] = array(
    '#title' => t('Refresh interval'),
    '#type' => 'item',
    '#markup' => $entity->refresh_interval == 0 ? t('On entity update') : format_interval($entity->refresh_interval),
  );

  $form['preview'] = array(
    '#title' => t('Content preview'),
    '#type' => 'fieldset',
  );

  $page_content = $entity->getPageContent();
  if ($page_content === NULL || $page_content === FALSE) {
    $page_content = '<em>' . t('The page content is empty. The page may be unpublished, or anonymous users may not have access to it. Check the logs for more details.') . '</em>';
  }
  else {
    // Add a space after closing HTML tags to prevent lumping together of
    // text in consecutive HTML tags.
    $page_content = preg_replace('#(</[a-zA-Z0-9_]+>)#', '$1 ', $page_content);
    $page_content = filter_xss_admin($page_content);
  }
  $form['preview']['page_content'] = array(
    '#type' => 'item',
    '#markup' => $page_content,
  );

  return $form;
}
